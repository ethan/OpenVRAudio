CCRMA OpenVRAudio Project
-------------------------

OpenVRAudio is an open source project that provides a callback for a monaural
audio buffer to be spatialized for binaural headphone output using a pre-rendered
or measured HRTF set. Various functions for generating HRTFs, as well as loading
and saving HRTF files.

The .HRTF File Spec
--------------------
Header:
    uint16: samplerate: actual sample rate these IRs are at.
    enum SampleType: sampletype: type we are using for our sample 
                        (float16, fixed32, etc) 
    enum PhaseResponse: phase: flag for whether our phase response is organized
    	 		(linear phase, minimum phase, or other)
    char: symmetry: one bit to designate whether this file only encompasses
                    one side of the head, and we should mirror it.
    uint16: ir_size: Number of samples for each seperate impulse response.
    uint16: az_res: Number of discrete IRs we have for our azimuth.
                    example: when az_res is four, and symmetry is 0, 
                    we have an impluse response every 90 degrees.
    uint16: el_res: Number of discrete IRs we have for each elevation.
                    example: when el_res is eight,
                    we have an impulse response every 45 degrees.
    
Data:
    sampletype[az_res][el_res][ir_size]: ir_set: set of all our pre-rendered 
                                                impulse responses.
    

    

Original OpenVR description:
------------------------------
OpenVR is an API and runtime that allows access to VR hardware from multiple 
vendors without requiring that applications have specific knowledge of the 
hardware they are targeting. This repository is an SDK that contains the API 
and samples. The runtime is under SteamVR in Tools on Steam. 

Documentation for the API is available in the wiki: https://github.com/ValveSoftware/openvr/wiki/API-Documentation

More information on OpenVR and SteamVR can be found on http://steamvr.com