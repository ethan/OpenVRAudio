//
//  HRTFFile.h
//  OpenVRAudioTestTool
//
//  Created by Ethan Geller on 3/12/16.
//  Copyright (c) 2016 CCRMA. All rights reserved.
//

#ifndef HRTFFile_h
#define HRTFFile_h
#include "OvraSampleTypes.h"


enum PhaseResponse {LINEAR_PHASE, MINIMUM_PHASE, OTHER_PHASE};

typedef float def_sample;

typedef struct _HRTFFileHeader {
    unsigned int sample_rate;
    enum SampleType sample_type;
    enum PhaseResponse phase;
    char symmetry;
    unsigned int ir_size, az_res, el_res;
} HRTFFileHeader;

typedef struct _HRTFFileDate {
    void* responses;
} HRTFFileData;

typedef struct _HRTFFile {
    HRTFFileHeader info;
    HRTFFileData impulse;
} HRTFFile;

#endif
