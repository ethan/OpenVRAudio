//
//  OvraSampleTypes.c
//  
//
//  Created by Ethan Geller on 3/22/16.
//
//

#include <stdio.h>
#include "OvraSampleTypes.h"

size_t ovraGetSampleTypeSize(enum SampleType type) {
    switch (type) {
        case FIXED16:
            return 2;
            break;
            
        case FIXED32:
            return 4;
            break;
            
        case FIXED64:
            return  8;
            break;
            
        case FLOAT:
            return sizeof(float);
            break;
            
        case DOUBLE:
            return sizeof(double);
            break;
            
        case LONG_DOUBLE:
            return sizeof(long double);
            break;
            
        default:
            return 0;
            break;
    }
};