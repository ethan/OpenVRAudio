//
//  OvraSampleTypes.h
//  OpenVRAudioTestTool
//
//  Created by Ethan Geller on 3/22/16.
//  Copyright (c) 2016 CCRMA. All rights reserved.
//

#ifndef OpenVRAudioTestTool_OvraSampleTypes_h
#define OpenVRAudioTestTool_OvraSampleTypes_h
#include <float.h>
#include <limits.h>

enum SampleType {FLOAT,DOUBLE,LONG_DOUBLE,FIXED16,FIXED32,FIXED64};

size_t ovraGetSampleTypeSize(enum SampleType type);

#endif
