//
//  HRTFFileHandler.c
//  
//
//  Created by Ethan Geller on 3/12/16.
//
//

#include "HRTFFileHandler.h"

size_t ovraGetHRTFFileDataSize(HRTFFileHeader* info) {
    size_t sz = ovraGetSampleTypeSize(info->sample_type);
    return info->ir_size * info->el_res * info->az_res * sz;
};

float* getFloat16IR(int az_idx, int el_idx, HRTFFile* src) {
    if (az_idx >= src->info.az_res || el_idx >= src->info.el_res) {
        printf(OVRA_IR_INDEX_OUT_OF_BOUNDS,az_idx,el_idx,src->info.az_res, src->info.el_res);
        return NULL;
    }
    size_t samp = ovraGetSampleTypeSize(src->info.sample_type);
    int ir_len = src->info.ir_size;
    size_t offset = ((ir_len*az_idx + ir_len*el_idx*(src->info.az_res)) * samp);
    return ((float*) (src->impulse.responses + offset));
};

double* getFloat32IR(int az_idx, int el_idx, HRTFFile* src) {
    if (az_idx >= src->info.az_res || el_idx >= src->info.el_res) {
        printf(OVRA_IR_INDEX_OUT_OF_BOUNDS,az_idx,el_idx,src->info.az_res, src->info.el_res);
        return NULL;
    }
    size_t samp = ovraGetSampleTypeSize(src->info.sample_type);
    int ir_len = src->info.ir_size;
    size_t offset = ((ir_len*az_idx + ir_len*el_idx*(src->info.az_res)) * samp);
    return ((double*) (src->impulse.responses + offset));
};

OVRA_ERR ovraLoadHRTFFile(const char* fpath, HRTFFile* dest) {
    FILE *f;
    f = fopen(fpath, "rb");
    if(f == NULL) return OVRA_FILE_NOT_FOUND;
    
    OVRA_ERR err = OVRA_SUCCESS;
    HRTFFileHeader* info = NULL;
    info = (HRTFFileHeader*) malloc(sizeof(HRTFFileHeader));
    fread((void*)info, sizeof(HRTFFileHeader), 1, f);
    size_t sz = ovraGetHRTFFileDataSize(info);
    dest->info = *info;
    dest->impulse.responses = malloc(sz);
    size_t numRead = fread(dest->impulse.responses, sz, 1, f);
    if(numRead!= 1) err = OVRA_FILE_DATA_CORRUPT;
    if (!err) err = fclose(f);
    else fclose(f);
    
    return err;
};

OVRA_ERR ovraSaveHRTFFile(const char* fpath, HRTFFile* src) {
    FILE* f;
    f = fopen(fpath, "wb");
    if(f == NULL) return OVRA_SOMETHING_WEIRD;
    
    OVRA_ERR err = OVRA_SUCCESS;
    fwrite((void*) &src->info, sizeof(HRTFFileHeader), 1, f);
    size_t sz = ovraGetHRTFFileDataSize(&src->info);
    void* resp = src->impulse.responses;
    size_t numRead = fwrite(resp, sz, 1, f);
    if(numRead!= 1) err = OVRA_FILE_DATA_CORRUPT;
    if(!err) err = fclose(f);
    else fclose(f);
    
    return err;
};

OVRA_ERR ovraResizeHRTFFile(HRTFFile* dest, int n) {
    size_t holdSz = ovraGetHRTFFileDataSize(&dest->info);
    HRTFFileData holder = dest->impulse;
    int oldIRLength = dest->info.ir_size;
    dest->info.ir_size += n;
    size_t newSz = ovraGetHRTFFileDataSize(&dest->info);
    dest->impulse.responses = malloc(newSz);
    int az_res = dest->info.az_res;
    int el_res = dest->info.el_res;
    if(n < 0) printf("Warning: destructively truncating file by %i samples...\n", -n);
    for (int i = 0; i < az_res; i++) {
        for(int k = 0; k < el_res; k++) {
            //copy buffers into new thing
            void* frm = holder.responses + ((oldIRLength*i + oldIRLength*k*az_res) * ovraGetSampleTypeSize(dest->info.sample_type));
            void* to = dest->impulse.responses + ((dest->info.ir_size*i + dest->info.ir_size*k*az_res) * ovraGetSampleTypeSize(dest->info.sample_type));
            if(n>0) memcpy(frm, to, holdSz);
            else memcpy(frm, to, newSz);
        }
    }
    free(holder.responses);
    return OVRA_SUCCESS;
};

OVRA_ERR ovraCopyHRTFFile(HRTFFile* src, HRTFFile* dest){
    dest->info = src->info;
    size_t sz = ovraGetHRTFFileDataSize(&dest->info);
    dest->impulse.responses  = malloc(sz);
    memcpy(dest->impulse.responses, src->impulse.responses, sz);
    return OVRA_SUCCESS;
};

void ovraCleanUpHRTFObject(HRTFFile* obj) {
    if(obj->impulse.responses != NULL) free(obj->impulse.responses);
    obj->impulse.responses = NULL;
};