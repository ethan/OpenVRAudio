//
//  OvraFloatConvolve.h
//  
//
//  Created by Ethan Geller on 3/25/16.
//
//

#ifndef ____OvraFloatConvolve__
#define ____OvraFloatConvolve__
#include <stdio.h>
#include <string.h>
#include "OvraDSPDefines.h"
#include "OvraFloatFFT.h"
#include "OvraSampleTypes.h"
#include "OvraUtils.h"

OVRA_ERR OvraFConvolve(float* source, int sourceLength, float* ir, float* holder, int irLength, float* output, float* leftover);

#endif /* defined(____OvraFloatConvolve__) */
