//
//  OvraFloatConvolve.c
//  
//
//  Created by Ethan Geller on 3/25/16.
//
//

#include "OvraFloatConvolve.h"

OVRA_ERR OvraFConvolve(float* source, int sourceLength, float* ir, float* holder, int irLength, float* output, float* leftover) {
    
    unsigned int fftSize = ovraNextPowerOf2(sourceLength + irLength - 1);
    ovraFRealFFT(source, fftSize/2, FFT_FORWARD);
    ovraFRealFFT(ir, fftSize/2, FFT_FORWARD);
    
    complexf* srcComp = (complexf *) source;
    complexf* irComp = (complexf *) ir;
    complexf* outComp = (complexf *) holder;
    
    float src_re, src_im, ir_re, ir_im;
    for(int i = 0; i < fftSize/2; i++) {
        src_re = srcComp[i].re; src_im = srcComp[i].im;
        ir_re = irComp[i].re; ir_im = srcComp[i].im;
        outComp[i].re = src_re * ir_re - src_im * ir_im;
        outComp[i].im = src_re * ir_im + src_im * ir_re;
    }
    
    ovraFRealFFT(holder, fftSize/2, FFT_INVERSE);
    memcpy(output, holder, sizeof(float)*sourceLength);
    memcpy(leftover, holder + sourceLength-1, sizeof(float)*(irLength-1));
    return OVRA_SUCCESS;
};