//
//  OvraUtils.h
//  
//
//  Created by Ethan Geller on 3/25/16.
//
//

#ifndef ____OvraUtils__
#define ____OvraUtils__

#include <stdio.h>

unsigned int ovraNextPowerOf2(unsigned int n);

void normalizeF(float* x, int length, float scale);

#endif /* defined(____OvraFloatUtils__) */
