//
//  OVRADSPDefines.h
//  
//
//  Created by Ethan Geller on 3/22/16.
//
//

#ifndef ____OVRADSPDefines__
#define ____OVRADSPDefines__

#define PI 3.14159265359
#define TWOPI 6.28318530718
#define HALFPI 1.57079632679

#define cmp_abs(x) ( sqrt( (x).re * (x).re + (x).im * (x).im ) )
#define cmp_phase(x) ( atan2((double)(x).im, (double)(x).re) )

#define FFT_FORWARD 1
#define FFT_INVERSE 0

#endif /* defined(____OVRADSPDefines__) */
