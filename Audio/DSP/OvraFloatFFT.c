//
//  OVRAFFT.c
//  
//
//  Created by Ethan Geller on 3/22/16.
//
//

#include "OvraFloatFFT.h"

float cmpf_abs(complexf x){ return sqrtf(x.re*x.re + x.im*x.im); };
float cmpf_phase(complexf x){ return atan2f(x.im, x.re); };

OVRA_ERR ovraFCosWindow(float* win, unsigned int length) {
    unsigned int i = 0;
    float phase=0,
          delta=2*PI/(float)length;
    
    for(i=0; i < length; i++) {
        win[i] = 0.5f*(1.0f-cosf(phase));
        phase+=delta;
    }
    
    return OVRA_SUCCESS;
};

OVRA_ERR ovraFApplyWindow(float* input, float* win, unsigned int length){
    unsigned int i;
    for(i=0; i < length; i++) input[i] *= win[i];
    return OVRA_SUCCESS;
};

OVRA_ERR ovraFRealFFT(float* x, unsigned int N, unsigned int forward){
    float c1, c2, h1r, h1i, h2r, h2i, wr, wi, wpr, wpi, temp, theta ;
    float xr, xi ;
    long i, i1, i2, i3, i4, N2p1 ;
    
    theta = PI/(float) N ;
    wr = 1.0f;
    wi = 0.0f;
    c1 = 0.5f;
    
    if( forward )
    {
        c2 = -0.5f;
        ovraFCompFFT(x, N, forward) ;
        xr = x[0] ;
        xi = x[1] ;
    }
    else
    {
        c2 = 0.5;
        theta = -theta ;
        xr = x[1];
        xi = 0.0f;
        x[1] = 0.0f;
    }
    
    wpr = (-2.*powf(sinf(0.5*theta), 2.0f)) ;
    wpi = sinf(theta) ;
    N2p1 = (N<<1) + 1 ;
    
    for( i = 0 ; i <= N>>1 ; i++ )
    {
        i1 = i<<1 ;
        i2 = i1 + 1 ;
        i3 = N2p1 - i2 ;
        i4 = i3 + 1 ;
        if( i == 0 )
        {
            h1r =  c1*(x[i1] + xr ) ;
            h1i =  c1*(x[i2] - xi ) ;
            h2r = -c2*(x[i2] + xi ) ;
            h2i =  c2*(x[i1] - xr ) ;
            x[i1] =  h1r + wr*h2r - wi*h2i ;
            x[i2] =  h1i + wr*h2i + wi*h2r ;
            xr =  h1r - wr*h2r + wi*h2i ;
            xi = -h1i + wr*h2i + wi*h2r ;
        }
        else
        {
            h1r =  c1*(x[i1] + x[i3] ) ;
            h1i =  c1*(x[i2] - x[i4] ) ;
            h2r = -c2*(x[i2] + x[i4] ) ;
            h2i =  c2*(x[i1] - x[i3] ) ;
            x[i1] =  h1r + wr*h2r - wi*h2i ;
            x[i2] =  h1i + wr*h2i + wi*h2r ;
            x[i3] =  h1r - wr*h2r + wi*h2i ;
            x[i4] = -h1i + wr*h2i + wi*h2r ;
        }
        
        wr = (temp = wr)*wpr - wi*wpi + wr ;
        wi = wi*wpr + temp*wpi + wi ;
    }
    
    if( forward )
        x[1] = xr ;
    else
        ovraFCompFFT(x, N, forward) ;
    
    return OVRA_SUCCESS;
};

OVRA_ERR ovraFCompFFT(float* x, unsigned int NC, unsigned int forward){
    float wr, wi, wpr, wpi, theta, scale;
    long mmax, ND, m, i, j, delta;
    ND = NC<<1;
    bit_reversef( x, ND );
    
    for( mmax = 2; mmax < ND; mmax = delta )
    {
        delta = mmax<<1;
        theta = TWOPI/(float)(forward? mmax : -mmax);
        wpr = (-2.*powf(sinf(0.5*theta), 2.0f));
        wpi = sinf(theta);
        wr = 1.0f;
        wi = 0.0f;
        
        for( m = 0 ; m < mmax ; m += 2 )
        {
            register float rtemp, itemp ;
            for( i = m ; i < ND ; i += delta )
            {
                j = i + mmax ;
                rtemp = wr*x[j] - wi*x[j+1] ;
                itemp = wr*x[j+1] + wi*x[j] ;
                x[j] = x[i] - rtemp ;
                x[j+1] = x[i+1] - itemp ;
                x[i] += rtemp ;
                x[i+1] += itemp ;
            }
            
            wr = (rtemp = wr)*wpr - wi*wpi + wr ;
            wi = wi*wpr + rtemp*wpi + wi ;
        }
    }
    
    // scale output
    scale = (float)(forward ? 1./ND : 2.) ;
    {
        register float *xi=x, *xe=x+ND ;
        while( xi < xe )
            *xi++ *= scale ;
    }
    
    return OVRA_SUCCESS;
};

void bit_reversef( float* x, long N )
{
    float rtemp, itemp ;
    long i, j, m ;
    for( i = j = 0 ; i < N ; i += 2, j += m )
    {
        if( j > i )
        {
            rtemp = x[j] ; itemp = x[j+1];
            x[j] = x[i] ; x[j+1] = x[i+1];
            x[i] = rtemp ; x[i+1] = itemp;
        }
        
        for( m = N>>1 ; m >= 2 && j >= m ; m >>= 1 )
            j -= m;
    }
}