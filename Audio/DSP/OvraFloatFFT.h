//
//  OVRAFFT.h
//  
//
//  Created by Ethan Geller on 3/22/16.
//
//

#ifndef ____OVRAFFT__
#define ____OVRAFFT__

#include <stdio.h>
#include "OpenVRAudioErrorCodes.h"
#include "OVRADSPDefines.h"
#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif
    typedef struct _complexf { float re ; float im ; } complexf;
    
    float cmpf_abs(complexf x);
    float cmpf_phase(complexf x);
    
    OVRA_ERR ovraFCosWindow(float* win, unsigned int length);
    OVRA_ERR ovraFApplyWindow(float* input, float* win, unsigned int length);
    OVRA_ERR ovraFRealFFT(float* x, unsigned int N, unsigned int forward);
    OVRA_ERR ovraFCompFFT(float* x, unsigned int NC, unsigned int forward);
    
    void bit_reversef( float* x, long N );
#ifdef __cplusplus
}
#endif
#endif /* defined(____OVRAFFT__) */
