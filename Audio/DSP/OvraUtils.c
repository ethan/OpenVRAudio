//
//  OvraFloatUtils.c
//  
//
//  Created by Ethan Geller on 3/25/16.
//
//

#include "OvraUtils.h"

unsigned int ovraNextPowerOf2(unsigned int n){
    unsigned int x = n;
    for(; x&= n-1; x=n);
    return x*2;
};

void normalizeF(float* x, int length, float scale){
    float max = 0;
    
    for(int i = 0; i < length; i++)
        if(fabs(x[i])>max)
            max = fabs(x[i]);
    
    for(int i = 0; i < length; i++) x[i] = (x[i]/max) * scale;
};