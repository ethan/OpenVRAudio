//
//  HRTFFileHandler.h
//  OpenVRAudioTestTool
//
//  Created by Ethan Geller on 3/12/16.
//  Copyright (c) 2016 CCRMA. All rights reserved.
//

#ifndef OpenVRAudioTestTool_HRTFFileHandler_h
#define OpenVRAudioTestTool_HRTFFileHandler_h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "HRTFFile.h"
#include "OpenVRAudioErrorCodes.h"
#ifdef __cplusplus
extern "C" {
#endif

size_t ovraGetHRTFFileDataSize(HRTFFileHeader* info);
    
float* getFloat16IR(int az_idx, int el_idx, HRTFFile* src);
double* getFloat32IR(int az_idx, int el_idx, HRTFFile* src);
    
OVRA_ERR ovraLoadHRTFFile(const char* fpath, HRTFFile* dest);
OVRA_ERR ovraSaveHRTFFile(const char* fpath, HRTFFile* src);

OVRA_ERR ovraResizeHRTFFile(HRTFFile* dest, int n);
OVRA_ERR ovraCopyHRTFFile(HRTFFile* src, HRTFFile* dest);
    
void ovraCleanUpHRTFObject(HRTFFile* obj);
    
#ifdef __cplusplus
}
#endif
#endif
