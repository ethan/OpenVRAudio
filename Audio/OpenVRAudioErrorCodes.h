//
//  OpenVRAudioErrorCodes.h
//  OpenVRAudioTestTool
//
//  Created by Ethan Geller on 3/12/16.
//  Copyright (c) 2016 CCRMA. All rights reserved.
//

#ifndef OpenVRAudioErrorCodes_h
#define OpenVRAudioErrorCodes_h

//error type
#define OVRA_ERR int

//error codes
#define OVRA_SUCCESS 0
#define OVRA_FILE_NOT_FOUND -1
#define OVRA_FILE_HEADER_CORRUPT -2
#define OVRA_FILE_DATA_CORRUPT -3
#define OVRA_SOMETHING_WEIRD -4

//text dumps
#define OVRA_IR_INDEX_OUT_OF_BOUNDS "OVRA ERROR: attempted to reach IR index out of bounds...\n\tazimuth index: %i\t elevation index: %i\n\tazimuth length: %i\televation length: %i"

#define OVRA_IR_SAMPLE_MISMATCH "Sample mismatch!\nat azimuth index: %i\t elevation index: %i\tsample: %i\n\t%0.2f\tvs\t%0.2f\n"

#endif
