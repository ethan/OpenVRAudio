//
//  main.cpp
//  OpenVRAudioTestTool
//
//  Created by Ethan Geller on 3/12/16.
//  Copyright (c) 2016 CCRMA. All rights reserved.
//

//#include <iostream>
#include "HRTFFileHandler.h"

//tests
#include "TestHRTFFileHandler.h"

void printSystemInformation() {
    printf("SYSTEM TYPE SIZES:\tint: %zu\tfloat: %zu\tlong double: %zu\n", sizeof(int), sizeof(float), sizeof(long double));
    
}

int main(int argc, const char * argv[]) {
    printSystemInformation();
    
    //Here come the tests!
    int err_count = 0;
    err_count+=OVRATestHRTFFileHandler();
    
    printf("Tests complete. Total Errors: %i\n", err_count);
    return err_count;
}
