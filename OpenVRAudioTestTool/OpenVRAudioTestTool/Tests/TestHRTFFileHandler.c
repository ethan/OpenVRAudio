//
//  TestHRTFFileHandler.c
//  
//
//  Created by Ethan Geller on 3/19/16.
//
//

#include "TestHRTFFileHandler.h"

int OVRACompareHRTFFileIRs(HRTFFile hrtf1, HRTFFile hrtf2) {
    //complete sample match check:
    unsigned int sample_mismatch_count = 0;
    for (int i = 0; i < hrtf1.info.az_res; i++) {
        for (int k = 0; k < hrtf1.info.el_res; k++) {
            float* ir1 = getFloat16IR(i, k, &hrtf1);
            float* ir2 = getFloat16IR(i, k, &hrtf2);
            for (int l = 0; l < hrtf1.info.ir_size; l++) {
                if (ir1[l] != ir2[l]) {
                    //printf(OVRA_IR_SAMPLE_MISMATCH,i,k,l, ir1[l], ir2[l]);
                    sample_mismatch_count++;
                }
            }
        }
    }
    
    return sample_mismatch_count;
};

int OVRATestHRTFFileHandler(void){
    
    //MARK: Initialization
    int err_count = 0;
    printf("HRTF File Handler check...\n");
    HRTFFile hrtf;
    hrtf.info.sample_rate = 48000;
    hrtf.info.sample_type = FLOAT;
    hrtf.info.az_res = 4;
    hrtf.info.el_res = 2;
    hrtf.info.ir_size = 8;
    hrtf.info.symmetry = 0;
    hrtf.info.phase = LINEAR_PHASE;
    
    size_t sz = ovraGetHRTFFileDataSize(&hrtf.info);
    hrtf.impulse.responses = malloc(sz);
    //make every hrtf white noise
    for (int i = 0; i < hrtf.info.az_res; i++) {
        for (int k = 0; k < hrtf.info.el_res; k++) {
            float* r_ptr = getFloat16IR(i, k, &hrtf);
            
            //impulse:
            //memset(r_ptr, 0, hrtf.info.ir_size*sizeof(float));
            //r_ptr[0] = 1.0f;
            
            //white noise:
            for(int l = 0; l < hrtf.info.ir_size; l++) {
                r_ptr[l] = (float)rand()/(float)(RAND_MAX/1);
            }
        }
    }
    
    //MARK: Tests
    OVRA_ERR err = OVRA_SUCCESS;
    err = ovraSaveHRTFFile("test.hrtf", &hrtf);
    if(err) {
        printf("Saving HRTF File failed: %i\n", err);
        err_count++;
    }
    HRTFFile hrtf2;
    err = ovraLoadHRTFFile("test.hrtf", &hrtf2);
    if(err) {
        printf("Loading HRTF File failed: %i\n", err);
        err_count++;
    }
    
    //headers match:
    if(hrtf.info.az_res != hrtf2.info.az_res) {
        printf("HRTF file azimuth resolution mismatch: %i vs %i\n",
               hrtf.info.az_res, hrtf2.info.az_res);
        err_count++;
    }
    if (hrtf.info.el_res != hrtf2.info.el_res) {
        printf("HRTF file elevation resolution mismatch: %i vs %i\n",
               hrtf.info.el_res, hrtf2.info.el_res);
        err_count++;
    }
    if (hrtf.info.ir_size != hrtf2.info.ir_size) {
        printf("HRTF file IR size mismatch: %i vs %i\n",
               hrtf.info.ir_size, hrtf2.info.ir_size);
        err_count++;
    }
    if (hrtf.info.phase != hrtf2.info.phase) {
        printf("HRTF file phase mismatch: %i vs %i\n",
               hrtf.info.phase, hrtf2.info.phase);
        err_count++;
    }
    if (hrtf.info.sample_rate != hrtf2.info.sample_rate) {
        printf("HRTF file sample rate mismatch: %i vs %i\n",
               hrtf.info.sample_rate, hrtf2.info.sample_rate);
        err_count++;
    }
    
    
    int sample_mismatch_count = OVRACompareHRTFFileIRs(hrtf, hrtf2);
    
    if(sample_mismatch_count) {
        printf("%i total mismatches of %i total samples.\n",
               sample_mismatch_count,
               hrtf.info.az_res * hrtf.info.el_res * hrtf.info.ir_size);
        err_count++;
    }
    
    //test IR resizing
    HRTFFile hrtf_copy;
    ovraCopyHRTFFile(&hrtf, &hrtf_copy);
    
    if (OVRACompareHRTFFileIRs(hrtf, hrtf_copy)) printf("ERROR: Copying hrtf files failed!\n");
    
    if(ovraResizeHRTFFile(&hrtf_copy, 256)){
        printf("Zero padding HRTF impulse responses failed\n");
        err_count++;
    };
    
    if(ovraResizeHRTFFile(&hrtf_copy, -256)) {
        printf("Truncating HRTF impulse responses failed\n");
        err_count++;
    }
    
    if (OVRACompareHRTFFileIRs(hrtf, hrtf_copy)) {
        printf("Resizing IR test failed\n");
        err_count++;
    }
    
    //MARK: Cleanup
    ovraCleanUpHRTFObject(&hrtf);
    ovraCleanUpHRTFObject(&hrtf2);
    ovraCleanUpHRTFObject(&hrtf_copy);
    return err_count;
};