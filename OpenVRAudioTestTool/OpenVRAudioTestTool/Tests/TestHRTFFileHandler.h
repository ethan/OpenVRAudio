//
//  TestHRTFFileHandler.h
//  
//
//  Created by Ethan Geller on 3/19/16.
//
//

#ifndef ____TestHRTFFileHandler__
#define ____TestHRTFFileHandler__

#include <stdio.h>
#include "HRTFFileHandler.h"
#if __cplusplus
extern "C" {
#endif
    
int OVRATestHRTFFileHandler(void);

    
#if __cplusplus
}
#endif
#endif /* defined(____TestHRTFFileHandler__) */
